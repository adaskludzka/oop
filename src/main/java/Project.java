import bookkeeping.contractor.Contractor;
import bookkeeping.contractor.ContractorOperations;
import bookkeeping.invoices.InvoicesOperations;
import bookkeeping.taxes.MonthSummary;
import bookkeeping.taxes.TaxCalculator;
import production.ProductOperations;
import staff.Employee;
import staff.EmployeeOperations;

import java.util.Scanner;

public class Project {

    Scanner sc = new Scanner(System.in);
    boolean exit = true;


    public void runProject() {
        while (exit) {

            System.out.println("Witaj w firmie ICE.");
            System.out.println("Zaloguj się");

            try (Scanner sc = new Scanner(System.in)) {
                System.out.print("Nazwa użytkownika: ");
                String userName = sc.nextLine();

                System.out.print("Hasło: ");
                String password = sc.nextLine();

                if ("admin".equals(userName) && "admin".equals(password)) {
                    System.out.println("Wybierz dział:");
                } else {
                    System.out.println("Niepoprawna nazwa użytkownika lub hasło.");
                }
            }

            System.out.println("Wybierz dział:");

            System.out.println("1. Księgowość. \n" +
                    "2. Kadry. \n" +
                    "3. Produkcja. \n");

            String decision = sc.next();

            if (decision.equals("1")) {
                bookkeepingModule();
            }

            if (decision.equals("2")) {
                staffModule();
            }

            if (decision.equals("3")) {
                productionModule();
            }


        }
    }


    private void productionModule() {
        System.out.println("Stan magazynowy produktów:");
        ProductOperations productOperations = new ProductOperations();
        productOperations.findAll();
    }

    private void staffModule() {
        System.out.println("Wybierz opcję:");
        System.out.println("1. Dodaj pracownika. \n" +
                "2. Usuń pracownika. \n" +
                "3. Edytuj dane danego pracownika.");

        String chooseFromStaff = sc.next();

        if (chooseFromStaff.equals("1")) {
            String aEmployee = sc.next();
            String bEmployee = sc.next();
            String cEmployee = sc.next();
            String dEmployee = sc.next();
            double eEmployee = sc.nextDouble();
            int fEmployee = sc.nextInt();
            EmployeeOperations employeeOperations = new EmployeeOperations();
            System.out.println("Dodaj nowego pracownika:");
            employeeOperations.save(new Employee(aEmployee, bEmployee, cEmployee, dEmployee, eEmployee, fEmployee));


        }
    }

        private void bookkeepingModule () {
            System.out.println("Wybierz opcję:");
            System.out.println("1. Kontrachenci. \n" +
                    "2. Faktury. \n" +
                    "3. Miesięczny podatek dochodowy.");

            String chooseFromBookkeeping = sc.next();

            if (chooseFromBookkeeping.equals("1")) {
                int aContractor = sc.nextInt();
                String bContractor = sc.next();
                String cContractor = sc.next();
                System.out.println("Dodaj kontachenta.");
                ContractorOperations operations = new ContractorOperations();
                System.out.println("Wpisz dane kontrachenta(id, nazwę, NIP):");
                operations.save(new Contractor(aContractor, bContractor, cContractor));

            }

            if (chooseFromBookkeeping.equals("2")) {
                System.out.println("Opcje faktur:");
                System.out.println("1. Dodaj. \n" +
                        "2. Zobacz faktury. \n" +
                        "3. Edytuj. \n" +
                        "4. Usuń.");
                if (chooseFromBookkeeping.equals("1")) {
                    long aInvoice = sc.nextLong();
                    double cInvoice = sc.nextDouble();
                    String bInvoice = sc.next();
                    double dInvoice = sc.nextDouble();
                    long eInvoice = sc.nextLong();
                    InvoicesOperations invoicesOperations = new InvoicesOperations();
                    System.out.println("Dane do faktury(id, sposób wypłaty, waluta, kwota, id kontachenta):");
                    //invoicesOperations.save(new Invoice(aInvoice,bInvoice,cInvoice,dInvoice,eInvoice));
                }
                if (chooseFromBookkeeping.equals("2")) {
                    InvoicesOperations invoicesOperations1 = new InvoicesOperations();
                    System.out.println("Faktury:");
                    invoicesOperations1.findAll();

                }

            }

            if (chooseFromBookkeeping.equals("3")) {
                TaxCalculator taxCalculator = new TaxCalculator();
                MonthSummary monthSummary = new MonthSummary();
                System.out.println("Podatek dochodowy do zapłaty:");
                System.out.println(taxCalculator.calculateTax(monthSummary.getTaxToPay()));

            }
        }
    }


