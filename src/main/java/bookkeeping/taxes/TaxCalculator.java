package bookkeeping.taxes;

public class TaxCalculator {

    private static final double TAX_FREE = 30_000;

    public double calculateTax(double income) {
        double incomeWithOutTaxFree = income - TAX_FREE;
        if (incomeWithOutTaxFree > 0 && incomeWithOutTaxFree <= 120_000) {
            return calculateFirstWayTax(incomeWithOutTaxFree);
        }
        if (incomeWithOutTaxFree > 120_000) {
            return calculateSecondWayTax(incomeWithOutTaxFree);
        }
        return 0;
    }

    private double calculateSecondWayTax(double incomeWithOutTaxFree) {
        double tax = 120_000 * 0.17;
        tax += (incomeWithOutTaxFree - 120_000) * 0.32;

        return tax;
    }

    private double calculateFirstWayTax(double incomeWithOutTaxFree) {
        return incomeWithOutTaxFree * 0.17;
    }

}
