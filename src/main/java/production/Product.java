package production;

public class Product {

    private String name;
    private int availability;

    public Product(String name, int availability) {
        this.name = name;
        this.availability = availability;
    }

    public String getName() {
        return name;
    }

    public int getAvailability() {
        return availability;
    }

    public Product(String line) {
        String[] split = line.split(",");
        this.name = split[0];
        this.availability = Integer.parseInt(split[1]);
    }
}
