package production;

import bookkeeping.invoices.Invoice;
import operations.CrudOperations;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProductOperations implements CrudOperations<Product> {

    public static final String PRODUCTS_CSV = "src/main/resources/products.csv";

    @Override
    public void save(Product type) {

    }

    @Override
    public void deleteById(long id) {

    }

    @Override
    public void update(long id, Product type) {

    }

    @Override
    public Product findById(long id) {
        return null;
    }

    @Override
    public List<Product> findAll() {
        List<Product> list = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(PRODUCTS_CSV))) {
            String line;
            while ((line = reader.readLine()) != null) {
                Product product = new Product(line);
                list.add(product);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }
}
