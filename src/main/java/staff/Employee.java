package staff;

public class Employee {


    private String first_name;
    private String last_name;
    private String email;
    private String gender;
    private double salary;
    private int age;

    public Employee(String first_name, String last_name, String email, String gender, double salary, int age) {

        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.gender = gender;
        this.salary = salary;
        this.age = age;
    }


    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getEmail() {
        return email;
    }

    public String getGender() {
        return gender;
    }

    public double getSalary() {
        return salary;
    }

    public int getAge() {
        return age;
    }

    public Employee(String line) {
        String[] split = line.split(",");
        this.first_name = split[0];
        this.last_name = split[1];
        this.email = split[2];
        this.gender = split[3];
        this.salary = Double.parseDouble(split[4]);
        this.age = Integer.parseInt(split[5]);
    }
}
